class Question {
  String q;
  bool a;

  Question({required this.q, required this.a});

  bool verifyAnswer(bool userAnswer) {
    return a == userAnswer;
  }

  String getQuestionText() {
    return q;
  }

  bool getQuestionAnswer() {
    return a;
  }
}
